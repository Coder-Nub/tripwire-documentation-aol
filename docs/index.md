# Welcome to the AoL Tripwire Guide

Here you will find guides about Tripwire and wormholes in general.

## Pages

* If you're just starting out with Tripwire, visit [Tripwire Initial Setup](tripwire_setup/)
* A few useful tips and tricks about Tripwire can be found here: [Tripwire Tips and Tricks](tripwire_tips/)
* A guide to identifying the destination class of a wormhole based on its color: [Wormhole Visual Identification](whvisual/)
* A few fun facts about wormholes: [Fun Facts About Wormholes](funfacts/)

## External resources

* [Wormhole Hunting by Longinius Spear - Eve Vegas 2015](https://www.youtube.com/watch?v=5ddR7W4LmR8)
* [A history of Wormhole space in EVE Online by ExookiZ - Eve Fanfest 2016](https://www.youtube.com/watch?v=X9P0f3KJCtE)
* [EVE-Survival : WormholeSpace (Community Updated)](http://eve-survival.org/wikka.php?wakka=WormholeSpace)
* [Rykki's Guide - Sleeper Sites - Last Updated 5/4/2017](https://docs.google.com/spreadsheets/d/17cNu8hxqJKqkkPnhDlIuJY-IT6ps7kTNCd3BEz0Bvqs/pubhtml#)

## Contributing

If you want to contribute to this guide, please contact *Caboosette* on Slack.